'use strict';

const path = require("path");

const express = require('express');
const bodyParser = require('body-parser');
const config = require('config');
const log4js = require('log4js');
const logger = require('./common/Logger');
const passport = require('passport');
const passportConfig = require('./common/passport')(passport);

const app = express();
app.use(log4js.connectLogger(logger, {
    level: 'auto',
    format: (req, res, format) => format(`:remote-addr ":method :url HTTP/:http-version" :status :content-length ":referrer" ":user-agent"`)
}));

const http_port = config.get('http_server.http_port');
app.listen(http_port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());

app.set('budgetsecret', config.get('passport.secret'));

// API
app.get('/api/v1/colors/get', require('./api/v1/colors/get'));
app.get('/api/v1/countries/get', require('./api/v1/countries/get'));
app.get('/api/v1/languages/get', require('./api/v1/languages/get'));
app.get('/api/v1/persons/get', require('./api/v1/persons/get'));
app.get('/api/v1/persons/get_count', require('./api/v1/persons/get_count'));

app.post('/login', (req, res) => {

});

// Views and Controllers
app.set('view engine', 'ejs');
app.get('/', require('./controllers/index'));
app.get('/login', require('./controllers/login'));

// Static
const pathToStaticDir = path.join(__dirname, "src");
app.use(express.static(pathToStaticDir));

logger.info(`app running on port http://localhost:${http_port}...`);
module.exports = app;