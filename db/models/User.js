'use strict';

const config = require('config');
const bcrypt = require('bcrypt');

module.exports = ({sequelize, Sequelize}) => {
    const UserModel = sequelize.define('users', {
        email: { type: Sequelize.STRING, allowNull: false, unique: true },
        password: { type: Sequelize.STRING, allowNull: false },
        firstName: { type: Sequelize.STRING, allowNull: false },
        secondName: { type: Sequelize.STRING, allowNull: false },
        telephone: { type: Sequelize.STRING },
    });

    UserModel.prototype.hashPassword = async function () {
        const rounds = config.get('bcrypt.rounds');
        const salt = await bcrypt.genSalt(rounds);
        this.password = await bcrypt.hash(this.password, salt);
    };

    UserModel.prototype.comparePassword = async function (password) {
        return await bcrypt.compare(password, this.password);
    };

    return UserModel;
};