'use strict';

module.exports = (options) => {
    const {sequelize, Sequelize, Country, Language, Color} = options;
    const Person = sequelize.define('persons', {
        name: { type: Sequelize.STRING, allowNull: false },
        country_id: { type: Sequelize.INTEGER },
        language_id: { type: Sequelize.INTEGER },
        color_id: { type: Sequelize.INTEGER }
    });
    Country.hasMany(Person);
    Person.belongsTo(Country);
    Language.hasMany(Person);
    Person.belongsTo(Language);
    Color.hasMany(Person);
    Person.belongsTo(Color);

    return Person;
};
