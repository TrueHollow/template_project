'use strict';

module.exports = ({sequelize, Sequelize}) => {
    return sequelize.define('countries', {
        name: { type: Sequelize.STRING, allowNull: false },
        short_value: { type: Sequelize.STRING, allowNull: false, unique: true }
    });
};
