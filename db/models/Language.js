'use strict';

module.exports = ({sequelize, Sequelize}) => {
    return sequelize.define('languages', {
        name: { type: Sequelize.STRING, allowNull: false },
        short_value: { type: Sequelize.STRING, allowNull: false, unique: true }
    });
};
