'use strict';

const {Language} = require('../index');

class Languages {
    static async getAllLanguages() {
        return await Language.findAll();
    }
}

module.exports = Languages;