'use strict';

const {Country} = require('../index');

class Countries {
    static async getAllCountries() {
        return await Country.findAll();
    }
}

module.exports = Countries;