'use strict';

const {Color} = require('../index');

class Colors {
    static async getAllColors() {
        return await Color.findAll();
    }
}

module.exports = Colors;