'use strict';

const {Person, Country, Language, Color, Sequelize} = require('../index');
const Op = Sequelize.Op;

const maxRowsLimit = 1000;
class Persons {
    static async getPersonsByOptions(options) {
        const query = {
            "attributes": ["id", "name"],
            "where": {},
            "include": [],
            "limit": maxRowsLimit
        };

        if (options.personsName) {
            query.where["name"] = {
                [Op.like]: `%${options.personsName}%`
            }
        }

        query.include.push({
            model: Country,
            attributes: { exclude: ["id", "short_value"] },
            where: (options.countries) ? {"short_value": options.countries} : {}
        });

        query.include.push({
            model: Language,
            attributes: { exclude: ["id", "short_value"] },
            where: (options.languages) ? {"short_value": options.languages} : {}
        });

        query.include.push({
            model: Color,
            attributes: { exclude: ["id", "short_value"] },
            where: (options.colors) ? {"short_value": options.colors} : {}
        });

        if (options.limit) {
            const limit = Number.parseInt(options.limit);
            if (Number.isInteger(limit) && limit < maxRowsLimit) {
                query.limit = limit;
            }
        }

        if (options.offset) {
            const offset = Number.parseInt(options.offset);
            if (Number.isInteger(offset)) {
                query.offset = offset;
            }
        }

        return await Person.findAll(query);
    }

    static async getPersonsByOptionsAndCount(options) {
        const query = {
            "attributes": ["id", "name"],
            "where": {},
            "include": [],
            "limit": maxRowsLimit
        };

        if (options.personsName) {
            query.where["name"] = {
                [Op.like]: `%${options.personsName}%`
            }
        }

        query.include.push({
            model: Country,
            attributes: { exclude: ["id"] },
            where: (options.countries) ? {"short_value": options.countries} : {}
        });

        query.include.push({
            model: Language,
            attributes: { exclude: ["id"] },
            where: (options.languages) ? {"short_value": options.languages} : {}
        });

        query.include.push({
            model: Color,
            attributes: { exclude: ["id"] },
            where: (options.colors) ? {"short_value": options.colors} : {}
        });

        if (options.limit) {
            const limit = Number.parseInt(options.limit);
            if (Number.isInteger(limit) && limit < maxRowsLimit) {
                query.limit = limit;
            }
        }

        if (options.offset) {
            const offset = Number.parseInt(options.offset);
            if (Number.isInteger(offset)) {
                query.offset = offset;
            }
        }

        return await Person.findAndCountAll(query);
    }
}

module.exports = Persons;