'use strict';

const config = require("config");
const Sequelize = require("sequelize");

let configurationDB = config.get("database");
if (configurationDB.logging === true) {
    const logger = require("../common/Logger");
    configurationDB = config.util.toObject(configurationDB);
    configurationDB.logging = (message) => {
        logger.debug(message);
    }
}

const sequelize = new Sequelize(configurationDB);

const db = {
    sequelize: sequelize,
    Sequelize: Sequelize
};

db.Color = require('./models/Color')(db);
db.Country = require('./models/Country')(db);
db.Language = require('./models/Language')(db);
db.Person = require('./models/Person')(db);
db.User = require('./models/User')(db);

module.exports = db;