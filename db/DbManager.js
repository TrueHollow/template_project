'use strict';

const {sequelize, Country, Language, Color, Person} = require('./index');

const config = require('config');

const countries = require('./test_data/countries');
const languages = require('./test_data/languages');
const colors = require('./test_data/colors');
const persons = require('./test_data/persons');

const logger = require('../common/Logger');

class DbManager {
    static async Init(options = config.get("DbManager.Init")) {
        logger.info('Creating tables (force %s)', options.force);
        await sequelize.sync(options);
    }

    static async SetTestData(addTestData = config.get("DbManager.SetTestData")) {
        if (addTestData) {
            logger.info('Adding test data');
            await Promise.all([Country.bulkCreate(countries), Language.bulkCreate(languages), Color.bulkCreate(colors)]);
            return Person.bulkCreate(persons);
        }
    }

    static async Close() {
        return sequelize.close();
    }
}

const checkDirectMode = () => {
    if (require.main === module) {
        logger.info('Running database initialization');
        DbManager.Init().then(DbManager.SetTestData).then(DbManager.Close)
            .then(() => {
                logger.info('Initialization finished');
            });
    } else {
        logger.info('Manager loaded as module');
    }
};

checkDirectMode();

module.exports = DbManager;