## Preparation

### Node version

Tested on

```sh
node --version
v11.10.1
```


### Create configuration file for your enviroment

Create custom configuration file in `config` folder with

```json
{
    "database": {
        "logging": false,
        "database": "db_name",
        "username": "test_user",
        "password": "test_user",
        "host": "localhost",
        "port": 3306,
        "dialect": "mysql",
        "pool": {
            "max": 5,
            "min": 0,
            "acquire": 30000,
            "idle": 10000
        },
        "operatorsAliases": false
    },
    "http_server": {
        "http_port": 3000
    },
    "log4js": {
        "appenders": {
            "out": {
                "type": "stdout"
            }
        },
        "categories": {
            "default": {
                "appenders": [
                    "out"
                ],
                "level": "info"
            }
        }
    },
    "DbManager": {
        "Init": {
            "force": true
        },
        "SetTestData": true
    }
}
```

and update environment variable `NODE_ENV`

### Install dependencies

Run command ```npm install```

### Database initialization

To init database tables you should execute ```npm run db_init```

### Running tests

Execute command ```npm test```

## Running app

Execute command ```npm start```