'use strict';

const PassportJWT = require('passport-jwt'),
    ExtractJWT = PassportJWT.ExtractJwt,
    Strategy = PassportJWT.Strategy,
    config = require('config'),
    {User} = require('../db');

module.exports = (passport) => {
    const parameters = {
        secretOrKey: config.get('passport.secret'),
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
    };
    passport.use(new Strategy(parameters, async (payload, done) => {
        const user = await User.findOne({ id: payload.id });
        return !!user;
    }));
};
