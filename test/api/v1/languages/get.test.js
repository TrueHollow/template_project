'use strict';

const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../../../index');
const {sequelize, Language} = require('../../../../db');

const urlPath = '/api/v1/languages/get';

const languages = require('../../../../db/test_data/languages');

const description = 'GET ' + urlPath;
describe(description, () => {
    before(async () => {
        await sequelize.sync({force: true});
        return Language.bulkCreate(languages);
    });
    it('test get languages data', async () => {
        const res = await chai.request(app).get(urlPath);
        expect(res).to.have.status(200);
        const result = res.body;
        expect(result).to.be.an('array').that.have.lengthOf(languages.length);
    });
});