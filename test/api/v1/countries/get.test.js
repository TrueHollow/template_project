'use strict';

const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../../../index');
const {sequelize, Country} = require('../../../../db');

const urlPath = '/api/v1/countries/get';

const countries = require('../../../../db/test_data/countries');

const description = 'GET ' + urlPath;
describe(description, () => {
    before(async () => {
        await sequelize.sync({force: true});
        return Country.bulkCreate(countries);
    });
    it('test get countries data', async () => {
        const res = await chai.request(app).get(urlPath);
        expect(res).to.have.status(200);
        const result = res.body;
        expect(result).to.be.an('array').that.have.lengthOf(countries.length);
    });
});