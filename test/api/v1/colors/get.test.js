'use strict';

const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../../../index');
const {sequelize, Color} = require('../../../../db');

const urlPath = '/api/v1/colors/get';

const colors = require('../../../../db/test_data/colors');

const description = 'GET ' + urlPath;
describe(description, () => {
    before(async () => {
        await sequelize.sync({force: true});
        return Color.bulkCreate(colors);
    });
    it('test get colors data', async () => {
        const res = await chai.request(app).get(urlPath);
        expect(res).to.have.status(200);
        const result = res.body;
        expect(result).to.be.an('array').that.have.lengthOf(colors.length);
    });
});