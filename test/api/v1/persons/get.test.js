'use strict';

const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../../../index');
const {sequelize, Country, Language, Color, Person} = require('../../../../db');

const urlPath = '/api/v1/persons/get';

const countries = require('../../../../db/test_data/countries');
const languages = require('../../../../db/test_data/languages');
const colors = require('../../../../db/test_data/colors');
const persons = require('../../../../db/test_data/persons');

const description = 'GET ' + urlPath;
describe(description, () => {
    before(async () => {
        await sequelize.sync({force: true});
        await Promise.all([Country.bulkCreate(countries), Language.bulkCreate(languages), Color.bulkCreate(colors)]);
        return Person.bulkCreate(persons);
    });
    it('test get persons data', async () => {
        const data = {
            "personsName": "val"
        };
        const res = await chai.request(app).get(urlPath).query(data);
        expect(res).to.have.status(200);
        const result = res.body;
        expect(result).to.be.an('array');
    });
});