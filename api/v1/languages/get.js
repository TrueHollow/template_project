'use strict';

const logger = require('../../../common/Logger');
const Languages = require('../../../db/queries/languages');

module.exports = async function(req, res) {
    try {
        const result = await Languages.getAllLanguages();
        return res.json(result);
    } catch (e) {
        logger.error(e);
        return res.status(500).send({err: "internal error"});
    }
};