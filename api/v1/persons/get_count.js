'use strict';

const logger = require('../../../common/Logger');
const Persons = require('../../../db/queries/persons');

module.exports = async function(req, res) {
    try {
        const options = req.query;
        const result = await Persons.getPersonsByOptionsAndCount(options);
        return res.json(result);
    } catch (e) {
        logger.error(e);
        return res.status(500).send({err: "internal error"});
    }
};