'use strict';

const logger = require('../../../common/Logger');
const Colors = require('../../../db/queries/colors');

module.exports = async function(req, res) {
    try {
        const result = await Colors.getAllColors();
        return res.json(result);
    } catch (e) {
        logger.error(e);
        return res.status(500).send({err: "internal error"});
    }
};