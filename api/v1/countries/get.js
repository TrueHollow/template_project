'use strict';

const logger = require('../../../common/Logger');
const Countries = require('../../../db/queries/countries');

module.exports = async function(req, res) {
    try {
        const result = await Countries.getAllCountries();
        return res.json(result);
    } catch (e) {
        logger.error(e);
        return res.status(500).send({err: "internal error"});
    }
};