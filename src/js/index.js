window.onload = async function() {
    const nameInput = document.getElementById("personsName");
    const countriesSelect = document.getElementById('countriesSelect');
    const languagesSelect = document.getElementById('languagesSelect');
    const colorsSelect = document.getElementById('colorsSelect');

    const getDataUrl = '/api/v1/persons/get';
    const getSearchResult = async () => {
        const data = {
            "personsName": nameInput.value,
            "countries": countriesSelect.value,
            "languages": languagesSelect.value,
            "colors": colorsSelect.value,
            "limit": 10
        };
        return new Promise((resolve, reject) => {
            $.get(getDataUrl, data, (queryResult, textStatus) => {
                if (textStatus === 'success') {
                    resolve(queryResult);
                } else {
                    reject(textStatus);
                }
            }, 'json');
        });
    };

    function autocomplete(inp) {
        var currentFocus;
        inp.addEventListener("input", async function (e) {
            var a, b, i, val = this.value;
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            this.parentNode.appendChild(a);
            const arr = await getSearchResult();
            for (i = 0; i < arr.length; i++) {
                b = document.createElement("div");
                const objArr = arr[i];
                const dataString = `${objArr.name}, ${objArr.country.name}, ${objArr.language.name}, ${objArr.color.name}`;

                const re = new RegExp(val, 'i');
                const strParts = dataString.split(re);
                b.appendChild(document.createTextNode(strParts[0]));

                for (let i = 1; i < strParts.length; ++i) {
                    const strongEl = document.createElement("strong");
                    strongEl.appendChild(document.createTextNode(val));
                    b.appendChild(strongEl);
                    b.appendChild(document.createTextNode(strParts[i]));
                }

                const hiddenInput = document.createElement("input");
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('value', objArr.name);
                b.appendChild(hiddenInput);
                b.addEventListener("click", function (e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }

        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }

    autocomplete(nameInput);

    const searchForm = document.getElementById('searchForm');
    const hiddenOffsetInput = document.getElementById('hiddenOffsetInput');
    const changeOffsetAndSubmit = (value) => {
        const intValue = Number.parseInt(hiddenOffsetInput.value);
        hiddenOffsetInput.value = intValue + value;
        searchForm.submit();
    };

    let button = document.getElementById('buttonPrevPage');
    if (button) {
        button.addEventListener('click', () => {
            changeOffsetAndSubmit(-5);
        });
    }
    button = document.getElementById('buttonNextPage');
    if (button) {
        button.addEventListener('click', () => {
            changeOffsetAndSubmit(5);
        });
    }
};