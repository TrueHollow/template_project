'use strict';
const Countries = require('../db/queries/countries');
const Languages = require('../db/queries/languages');
const Colors = require('../db/queries/colors');
const Persons = require('../db/queries/persons');

const logger = require('../common/Logger');
const resultsPerPage = 5;

const validateOptions = (options) => {
    let offset = Number.parseInt(options.offset);
    if (!Number.isInteger(offset) || offset < 0) {
        offset = 0;
    }
    options.limit = resultsPerPage;
    options.offset = offset;
    return options;
};

module.exports = async function(req, res) {
    try {
        const options = validateOptions(req.query);
        const result = await Promise.all([
            Countries.getAllCountries(),
            Languages.getAllLanguages(),
            Colors.getAllColors(),
            Persons.getPersonsByOptionsAndCount(options)
        ]);
        const page = Math.ceil(options.offset / resultsPerPage + 1);
        let totalPages = Math.ceil(result[3].count / resultsPerPage);
        if (totalPages === 0) {
            totalPages = 1;
        }

        const model = {
            title: "EJS db queries.",
            countries: result[0],
            languages: result[1],
            colors: result[2],
            persons: result[3],
            options: options,
            page: page,
            totalPages: totalPages,
            resultsPerPage: resultsPerPage
        };
        res.render('pages/index', model);
    } catch (e) {
        logger.error('Error! (%s)', e);
        res.status(500).render('pages/500');
    }
};