'use strict';

module.exports = async function(req, res) {
    try {
        const model = {
            title: "EJS db queries."
        };
        res.render('pages/login', model);
    } catch (e) {
        logger.error('Error! (%s)', e);
        res.status(500).render('pages/500');
    }
};